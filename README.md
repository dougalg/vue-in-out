# vue-in-out

A vue directive factory and directives to simplify in/out state changes such as mousein/mouseout, focusin/focusout, etc.

## Installation

`npm install vue-in-out`

## Browser/polyfill requirements

- `classList` API
- Object destructuring
- Arrow functions
- Template strings

*Note:* In short, you may want to transpile es6 and polyfill the `classList` API if you are supporting older browsers, or Safari.

## Directives

Some simple directives to simplify user interaction state management. These are essentially a shorthand for adding multiple bindings that are commonly used together.

### vue-inner-focus

Tracks focusin/focusout. On focusin adds the `focused` class to the element with the directive. On focusout, removes it. Can take a callback event which is called with either `true` (focusin), or `false` (focusout).

```
import innerFocus from 'vue-in-out/src/inner-focus';

export myComponent = {
    directives: {
        'inner-focus': innerFocus,
    },
    template: `
        <div class="testDiv" v-inner-focus>
            <button class="testButton"></button>
        </div>`
};
```

#### Focusout Timeout

To accomodate some browser quirks and to ensure that the `outEvent` timeout has a chance to be cleared by `inEvent`s there is a 50ms delay before the `outEvennt` fires. If this does not suit your needs, please see the directive factory below.

#### Customization

This simply allows a `focused` class to be added or removed. Custom classes and callbacks are supported:

```
import innerFocus from 'vue-in-out/src/inner-focus';

export myComponent = {
    directives: {
        'inner-focus': innerFocus,
    },
    template: `
        <div class="testDiv" v-inner-focus:customClass="onFocusChange">
            <button class="testButton"></button>
        </div>`,
    methods: {
        onFocusChange(isFocused) {
            console.log('This is focused', isFocused);
        }
    },
};
```

### vue-hovered

```
import innerFocus from 'vue-in-out/hovered';
```

Default class is `hovered`. Otherwise, behaves identically to the inner-focus directive.

## Directive Factory

This allows easy creation of directives similar to those above. For example, the inner-focus directive is simply:

```
import { inOutDirectiveFactory } from 'vue-in-out/src/in-out-factory';

export const DEFAULT_CLASS = 'focused';

export default inOutDirectiveFactory({
    inEvent: 'focusin',
    outEvent: 'focusout',
    activeClass: DEFAULT_CLASS,
});
```

### Options

| Key | Default | Description |
|-----|---------|-------------|
| `inEvent` | None | The event such as `focusin` to start with |
| `outEvent` | None | The event such as `focusout` to end with |
| `activeClass` | None | The class to attach when in the `in` state |
| `defaultCallback` | () => {} | A callback that will be called with `true` when moving to the `in` state and `false` when moving to the `out` state. This will typically be overridden in the view with callback bound to the Vue instance (eg `onFocusChange` above). |
| `outTimeout` | 50 | The time to wait between receiving the `out` event and actually triggering the callback and removing the active class. |
