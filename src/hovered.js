import {inOutDirectiveFactory} from './in-out-factory';

export const DEFAULT_CLASS = 'hovered';

export default inOutDirectiveFactory({
    inEvent: 'mouseover',
    outEvent: 'mouseout',
    activeClass: DEFAULT_CLASS,
});
