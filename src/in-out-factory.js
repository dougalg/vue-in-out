export const DEFAULT_TIMEOUT = 50;
export const DEFAULT_CALLBACK = () => {};
export const inName = name => `$${name}_inListener`;
export const outName = name => `$${name}_outListener`;
export const timerName = name => `$${name}_tid`;

export const inOutDirectiveFactory = options => {
    const {
        inEvent,
        outEvent,
        activeClass,
        defaultCallback = DEFAULT_CALLBACK,
        outTimeout = DEFAULT_TIMEOUT,
    } = options;
    return {
        inserted(el, {name, value = defaultCallback, arg = activeClass}) {
            el[inName(name)] = () => {
                clearTimeout(el[timerName(name)]);
                el.classList.add(arg);
                value(true);
            };

            el.addEventListener(inEvent, el[inName(name)], {passive: true});

            el[outName(name)] = () => {
                clearTimeout(el[timerName(name)]);
                el[timerName(name)] = setTimeout(() => {
                    el.classList.remove(arg);
                    value(false);
                }, outTimeout);
            };

            el.addEventListener(outEvent, el[outName(name)], {passive: true});
        },
        unbind(el, {name}) {
            clearTimeout(el[timerName(name)]);
            delete el[timerName(name)];
            el.removeEventListener(inEvent, el[inName(name)]);
            delete el[inName(name)];
            el.removeEventListener(outEvent, el[outName(name)]);
            delete el[outName(name)];
        },
    };
};
