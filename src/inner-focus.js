import {inOutDirectiveFactory} from './in-out-factory';

export const DEFAULT_CLASS = 'focused';

export default inOutDirectiveFactory({
    inEvent: 'focusin',
    outEvent: 'focusout',
    activeClass: DEFAULT_CLASS,
});
