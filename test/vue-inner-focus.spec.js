import Vue from 'vue';
import innerFocus, {DEFAULT_CLASS} from '../src/inner-focus';
import {DEFAULT_TIMEOUT} from '../src/in-out-factory';
import * as components from './vue-inner-focus/components';

const getRenderedComponent = options => {
    const Constructor = Vue.extend(options);
    const vm = new Constructor();
    vm.$mount();
    return vm;
};

const getInnerFocusComponent = (...options) => {
    options = Object.assign({}, ...options, {
        directives: {
            'inner-focus': innerFocus,
        },
    });
    return getRenderedComponent(options);
};

const promiseTimeout = ts => new Promise(resolve => {
    setTimeout(() => resolve(), ts);
});

const runThreeAssertionTest = async (view, testInitial, testAfterFocus, testAfterBlur) => {
    testInitial();
    document.body.appendChild(view.$el);

    await Vue.nextTick();

    view.$el.querySelector('button').focus();

    await Vue.nextTick();

    testAfterFocus();
    view.$el.querySelector('button').blur();

    await promiseTimeout(DEFAULT_TIMEOUT);
    await Vue.nextTick();

    testAfterBlur();

    document.body.removeChild(view.$el);
    view.$destroy();
    await Vue.nextTick();
};

const testClass = (theClass, view) => {
    return runThreeAssertionTest(
        view,
        () => expect(view.$el.classList.contains(theClass), 'Focus class was added before focusing element').to.equal(false),
        () => expect(view.$el.classList.contains(theClass), 'Focus class was not added after focus').to.equal(true),
        () => expect(view.$el.classList.contains(theClass), 'Focus class was not removed after blur').to.equal(false),
    );
};

const testProperty = async (theProperty, theSpy, view) => {
    return runThreeAssertionTest(
        view,
        () => expect(theSpy.calledWithExactly(), 'Focus callback was called before focusing element'),
        () => expect(theSpy.calledWithExactly(true), 'Focus callback was not called with true after focus'),
        () => expect(theSpy.calledWithExactly(true, false), 'Focus callback was not called with false after blur'),
    );
};

describe('vue-inner-focus', () => {
    describe('focused class', () => {
        it('should add focused class when focused and remove it when focus lost', async () => {
            const view = getInnerFocusComponent(components.simple());
            await testClass(DEFAULT_CLASS, view);
        });
        it('should add custom focused class when focused and remove it when focus lost', async () => {
            const customClass = 'customFocusClass';
            const view = getInnerFocusComponent(components.customClass(customClass));
            await testClass(customClass, view);
        });
    });
    describe('focused callback', () => {
        it('should call callback with true when focused, and false when focus is lost', async () => {
            const cbName = 'theCb';
            const cb = sinon.spy();
            const view = getInnerFocusComponent(components.customCallback());
            await testProperty(cbName, cb, view);
        });
    });
});
