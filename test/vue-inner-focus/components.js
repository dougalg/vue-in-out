export const simple = () => ({
    template: `
        <div class="testDiv" v-inner-focus>
            <button class="testButton"></button>
        </div>`,
});

export const customClass = customClass => ({
    template: `
        <div class="testDiv" v-inner-focus:${customClass}>
            <button class="testButton"></button>
        </div>`,
});

export const customCallback = (customCallbackName, customCallback) => ({
    methods: {
        [customCallbackName]: customCallback,
    },
    template: `
        <div class="testDiv" v-inner-focus="${customCallback}">
            <button class="testButton"></button>
        </div>`,
});

export const customPropAndClass = (customClass, customProp) => ({
    template: `
        <div class="testDiv" v-inner-focus:${customProp}="'${customClass}'">
            <button class="testButton"></button>
        </div>`,
});
