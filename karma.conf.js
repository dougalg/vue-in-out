const webpack = require('./webpack.config.js');

module.exports = function (config) {
    config.set({
        // To run in additional browsers:
        // 1. Install corresponding karma launcher
        //    http://karma-runner.github.io/0.13/config/browsers.html
        // 2. Add it to the `browsers` array below.
        browsers: ['PhantomJS'],
        frameworks: ['mocha', 'sinon-chai', 'phantomjs-shim'],
        reporters: ['spec', 'coverage'],
        files: ['./test/index.js'],
        preprocessors: {
            './test/index.js': ['webpack', 'sourcemap'],
        },
        webpack,
        webpackMiddleware: {
            noInfo: true,
        },
        coverageReporter: {
            dir: './coverage',
            reporters: [
                {
                    type: 'lcov',
                    subdir: '.',
                },
                {
                    type: 'text-summary',
                },
            ],
        },
        singleRun: true,
    });
};
