global.__basedir = __dirname;
const path = require('path');

module.exports = {
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js',
        },
        modules: [
            'node_modules',
        ],
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                include: [path.resolve(__basedir, 'src'), path.resolve(__basedir, 'test')],
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            babelrc: false,
                            presets: ['babel-preset-es2015', 'babel-preset-es2016', 'babel-preset-es2017'],
                        },
                    },
                ],
            },
            {
                test: /\.js$/,
                include: path.join(__basedir, 'src'),
                use: [
                    {
                        loader: 'istanbul-instrumenter-loader',
                        options: {
                            cacheDirectory: true,
                            esModules: true,
                        },
                    },
                ],
            },
        ],
    },
};
